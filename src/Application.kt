package com.example

import com.common.*

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*

import java.lang.Exception
import java.lang.NumberFormatException
import java.math.BigDecimal
import java.math.RoundingMode

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CORS) {
        method(HttpMethod.Options)
        anyHost()
    }
    install(ContentNegotiation) {
        gson {
        }
    }

    val client = HttpClient(Apache) {
    }

    routing {
        get("/salary/{salary}") {
            val salary = call.parameters["salary"]?.toDouble()
            try {
                if (salary !== null) {
                    val inssZone: InssOption? = findZone(salary, inssTable)
                    val irZone: IrOption? = findZone(salary, irTable)
                    val salaryLessInss = salary - inssTaxCalc(inssZone, salary)
                    val salaryLessIr = salaryLessInss - irTaxCalc(irZone, salaryLessInss)
                    println("Salário com desconto de INSS e IR: $salaryLessIr")
                    call.respond(mapOf("liquidSalary" to salaryLessIr))
                }
            } catch (e: Exception) {
                println("Salário deve ser um número no formato 1111.11")
                call.respond(mapOf("message" to "Salário deve ser um número no formato 1111.11"))
            }
        }
    }
}

