package com.common

import kotlin.Double.Companion.POSITIVE_INFINITY

interface ZoneOption {
    val min: Double
    val max: Double
    val percentage: Double
}

class InssOption(
    override val min: Double,
    override val max: Double,
    override val percentage: Double,
    val maxTax: Double? = null
) : ZoneOption {
    override fun toString(): String = "min: $min, max: $max, percentage: $percentage, maxTax: $maxTax"
}

class IrOption (
    override val min: Double,
    override val max: Double,
    override val percentage: Double,
    val deduction: Double
) : ZoneOption {
    override fun toString(): String = "min: $min, max: $max, percentage: $percentage, deduction: $deduction"
}

val inssTable = arrayOf(
    InssOption(5839.45, POSITIVE_INFINITY,0.11,642.34),
    InssOption(2919.73,5839.45,0.11,null),
    InssOption(1751.82,2919.72,0.09,null),
    InssOption(0.0,1751.81,0.08,null)
)

val irTable = arrayOf(
    IrOption(4664.69, POSITIVE_INFINITY,0.275,869.36),
    IrOption(3751.06,4664.68,0.225,636.13),
    IrOption(2826.66,3751.05,0.15,354.80),
    IrOption(1903.99,2826.65,0.075,142.80),
    IrOption(0.0,1903.98,0.0,0.0)
)