package com.common

import java.math.BigDecimal
import java.math.RoundingMode.HALF_UP

fun round(value: Double): BigDecimal = BigDecimal(value).setScale(2, HALF_UP)
fun <T: ZoneOption> findZone(value: Double, table: Array<T>) : T? = table.find { zone -> value >= zone.min && value <= zone.max }
fun inssTaxCalc(zone: InssOption?, value: Double): Double = if( zone?.maxTax === null ) value * (zone?.percentage ?: 0.0) else zone.maxTax
fun irTaxCalc(zone: IrOption?, value: Double): Double = round(value * (zone?.percentage ?: 0.0) - (zone?.deduction ?: 0.0)).toDouble()